import * as Home from '../components/home/action-creator';
// import * as StatLoL from '../components/statLoL/action-creator';
import * as Auth from '../components/auth/action-creator';
import * as Menu from '../components/basePage/action-creator';
import * as EventTracker from '../components/eventTracker/action-creator';
import * as Log from '../components/logger/action-creator';

import { actionDispatcher } from '../store';

/** Aggrégation des action creator */
const sources = [
    Auth,
    Menu,
    EventTracker,
    Log,
    Home,
]

/** Map des actions */
const main: { [key: string]: () => void } = {};

interface ActionSource {
    [key: string]: () => void
}

// Conversion des actions
sources.forEach(importActions => {

    /** Source d'action */
    const source = importActions as any as ActionSource;

    // Parcours des actions de la source et conversion
    Object.keys(source).forEach(actionName => {
        const action = source[actionName];
        if (typeof action === 'function') {
            main[actionName] = actionDispatcher(action);
        }
    });
});

export const actions = main;