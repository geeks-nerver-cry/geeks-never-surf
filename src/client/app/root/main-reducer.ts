import { Reducer } from "../base/reducer";

import IndexReducer from '../components/index-reducer';

const regexNodeName = /(.*)Reducer/;


// Redux reducer
export const mainReducer: Reducer = (state, action) => {

    IndexReducer.forEach(item => {
        if (regexNodeName.test(item.name)) {
            const nodeName = regexNodeName.exec(item.name);
            state[nodeName[1]] = item(state[nodeName[1]], action);
        } else {
            console.error('Reducer ignoré: ' + item.name);
        }
    });

    return state;
}