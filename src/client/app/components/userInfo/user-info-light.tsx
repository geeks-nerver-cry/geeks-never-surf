import { RouterProps } from "../../base/router";
import * as React from "react";
import './style.less';

export interface UserInfoLightProps {
    signOut: () => void;
}

export class UserInfoLight extends React.Component<UserInfoLightProps, undefined> {
    public render() {

        return (
            <div className="user-info" onClick={() => this.props.signOut()}>
                <i className="fa fa-power-off"></i>
            </div>
        );
    }
}