import { Reducer } from "../../base/reducer";
import { ActionType } from './action-creator';

// Event
export type UserState = {};

const EventReducer: Reducer = (state: UserState, action) => {
    let newState: UserState;

    switch (action.type) {

        case ActionType.ReceiveUser:
            break;

        default:
            newState = state || {};
    }

    return newState;
};