export enum Profile {
    DryEyes,
    TearDrop,
}

export interface UserInfo {
    name: string;
    firstName: string;
    nickname: string;
    profile: Profile;
}