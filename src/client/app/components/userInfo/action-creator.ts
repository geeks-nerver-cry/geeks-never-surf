import { Action } from "../../base/reducer";
import { ReferencePathType, FirebaseEvent, getReference, receiveData } from "../../base/firebase";

enum ActionType {
    ReceiveUser = 'RECEIVE_USER'
}


/**
 * Souscrit à la base de données pour les évènement
 */
export const subcribeEvent = function () {

    return (dispatch: (action: Action<any>) => void) => {

        //#region Users
        // Reference to the /events/ database path.
        const usersRef = getReference(ReferencePathType.Users);

        // Make sure we remove all previous listeners.
        usersRef.off();

        // Loads events and listen for new ones.
        usersRef.on(FirebaseEvent.ChildAdded, data => dispatch(receiveData(ActionType.ReceiveUser, data)));
        usersRef.on(FirebaseEvent.ChildChanged, data => dispatch(receiveData(ActionType.ReceiveUser, data)));
        //#endregion
    };
};

/**
 * Supprime les handler pour les users
 */
export const removeSubcribeEvent = function () {

    return () => {

        // Reference to the /messages/ database path.
        const usersRef = getReference(ReferencePathType.Users);
        // Make sure we remove all previous listeners.
        usersRef.off();
    };
};
