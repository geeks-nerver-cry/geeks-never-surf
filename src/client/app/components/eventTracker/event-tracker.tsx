import * as React from "react";
import './style.less';
import { RouterProps } from "../../base/router";
import { EventState, GameState } from "./reducer";
import { Tile } from "./tile";
import { Details } from './details';
import { Event, defaultEvent, Game } from './model';
import { Link } from "react-router-dom";

// 'EventTrackerProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
export class EventTracker extends React.Component<RouterProps, never> {


    public componentWillMount() {
        const actions = this.props.actions;
        actions.subcribeEvent();
    }

    private scrollEventTiles(way: 1|-1) {
        this.refs.scroller && ((this.refs.scroller as any).scrollLeft += (200 * way));
    }

    render() {
        const state: EventState = this.props.state.Event;
        const games: GameState = this.props.state.Game;
        const eventTypes: string[] = this.props.state.EventType;
        const actions = this.props.actions;
        const activeEventKey = this.props.match.params.id;
        const activeEvent = activeEventKey === 'new' ? defaultEvent : state[activeEventKey];

        return (
            <div className="event-tracker">
                <header>
                    <div className="event-tiles">
                        <div className="scroll-event">
                            <button onClick={() => this.scrollEventTiles(1)}><i className="fa fa-chevron-left"></i></button>
                        </div>
                        <div className="tile-scrollable" ref='scroller'>
                            <div className="tile-wrapper">
                                {state &&
                                    Object.keys(state).map((key: string) => {
                                        const event = state[key];
                                        event.isActive = key === activeEventKey;
                                        return (
                                            <Tile event={event} key={key} eventId={key} clickHandler={() => actions.detailEvent(key)} />
                                        );
                                    })
                                }
                            </div>
                        </div>
                        <div className="scroll-event">
                            <button onClick={() => this.scrollEventTiles(-1)}><i className="fa fa-chevron-right"></i></button>
                        </div>
                    </div>
                    <span className="add-event" onClick={() => actions.detailEvent()}>
                        <Link to='/event-tracker/new'><i className="fa fa-plus "></i></Link>
                    </span>
                </header>
                {activeEvent &&
                    <Details event={activeEvent} eventId={activeEventKey} cancelHandler={actions.cancelEvent} saveHandler={actions.saveEvent} suggestions={games} handleAddGame={(game: Game) => actions.saveGame(game)} />
                }
            </div>
        );
    }
}