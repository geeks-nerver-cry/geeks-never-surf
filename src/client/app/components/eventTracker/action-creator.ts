import { Action } from "../../base/reducer";
import { database, FirebaseEvent, ReferencePathType, DataSnapshot } from '../../base/firebase';
import { Event, mapToEventDTO, EventDTO, GameDTO, Game, mapToGameDTO } from './model';


export const DETAIL_EVENT = 'DETAIL_EVENT';
export const detailEvent = function (eventId: string) {

    let action: Action<{ eventId: string }> = {
        type: DETAIL_EVENT,
        data: {
            eventId
        }
    };

    return action;
};

export const CANCEL_EVENT = 'CANCEL_EVENT';
export const cancelEvent = function () {

    let action: Action<void> = {
        type: CANCEL_EVENT
    };

    return action;
};

export interface SavedEvent {
    event: Event;
    key: string;
}

export const SAVED_EVENT = 'SAVED_EVENT';
export const savedEvent = function (event: Event, key: string) {

    let action: Action<SavedEvent> = {
        type: SAVED_EVENT,
        data: {
            event,
            key
        }
    };

    return action;
};

export const saveEvent = function (key: string, event: Event) {

    return (dispatch: (action: Action<any>) => Action<any>) => {

        event.isActive = true;
        if (key !== 'new') {
            database.ref('events/' + key)
                .set(mapToEventDTO(event))
                .then(() => {
                    dispatch(savedEvent(event, key));
                })
                .catch(() => {
                    console.log('Faudrait réellement avoir une gestion d\'erreur correcte ... Au fait, la sauvegarde de l\'event a plantée.');
                });
        } else {
            database.ref('events')
                .push(mapToEventDTO(event))
                .then((newEvent: DataSnapshot<EventDTO>) => {
                    dispatch(savedEvent(event, newEvent.key));
                });

        }
    };
};

export const RECEIVE_EVENT = 'RECEIVE_EVENT';
const receiveEvent = (snapshot: DataSnapshot<EventDTO>): Action<any> => {
    return {
        type: RECEIVE_EVENT,
        data: snapshot
    };
};

export const RECEIVE_EVENT_TYPE = 'RECEIVE_EVENT_TYPE';
const receiveEventType = (snapshot: DataSnapshot<string>): Action<any> => {
    return {
        type: RECEIVE_EVENT_TYPE,
        data: snapshot
    };
};

export const RECEIVE_GAME = 'RECEIVE_GAME';
const receiveGame = (snapshot: DataSnapshot<GameDTO>): Action<any> => {
    return {
        type: RECEIVE_GAME,
        data: snapshot
    };
};

export interface SavedGame {
    game: Game;
}


export const saveGame = function (game: Game) {

    return (dispatch: (action: Action<any>) => Action<any>) => {

        if (game.id === 'new' || !game.id) {
            database.ref('games')
                .push(mapToGameDTO(game))
                .then((newGame: DataSnapshot<GameDTO>) => {
                    game.id = newGame.key;
                    dispatch(savedGame(game));
                });
        } else {
            database.ref('games/' + game.id)
                .set(mapToGameDTO(game))
                .then(() => {
                    dispatch(savedGame(game));
                })
                .catch(() => {
                    console.log('Faudrait réellement avoir une gestion d\'erreur correcte ... Au fait, la sauvegarde du game a plantée.');
                });
        }
    };
};

export const SAVED_GAME = 'SAVED_GAME';
export function savedGame(game: Game) {

    let action: Action<SavedGame> = {
        type: SAVED_GAME,
        data: {
            game
        }
    };

    return action;
};


/**
 * Souscrit à la base de données pour les évènement
 */
export const subcribeEvent = function () {

    return (dispatch: (action: Action<any>) => void) => {

        //#region Event
        // Reference to the /events/ database path.
        const eventsRef = database.ref(ReferencePathType.Events);

        // Make sure we remove all previous listeners.
        eventsRef.off();

        // Loads events and listen for new ones.
        eventsRef.on(FirebaseEvent.ChildAdded, data => dispatch(receiveEvent(data)));
        eventsRef.on(FirebaseEvent.ChildChanged, data => dispatch(receiveEvent(data)));
        //#endregion

        //#region EventType
        // Reference to the /eventTypes/ database path.
        const eventTypesRef = database.ref(ReferencePathType.EventTypes);
        // Make sure we remove all previous listeners.
        eventTypesRef.off();

        // Loads events and listen for new ones.
        eventTypesRef.on(FirebaseEvent.ChildAdded, data => dispatch(receiveEventType(data)));
        eventTypesRef.on(FirebaseEvent.ChildChanged, data => dispatch(receiveEventType(data)));
        //#endregion

        //#region Games
        // Reference to the /games/ database path.
        const gamesRef = database.ref(ReferencePathType.Games);
        // Make sure we remove all previous listeners.
        gamesRef.off();

        // Loads events and listen for new ones.
        gamesRef.on(FirebaseEvent.ChildAdded, data => dispatch(receiveGame(data)));
        gamesRef.on(FirebaseEvent.ChildChanged, data => dispatch(receiveGame(data)));
        //#endregion
    };
};

/**
 * Souscrit à la base de données pour les évènement
 */
export const removeSubcribeEvent = function () {

    return () => {

        // Reference to the /messages/ database path.
        const eventsRef = database.ref(ReferencePathType.Events);
        // Make sure we remove all previous listeners.
        eventsRef.off();
        // Reference to the /messages/ database path.
        const eventTypesRef = database.ref(ReferencePathType.EventTypes);
        // Make sure we remove all previous listeners.
        eventTypesRef.off();
    };
};

const getEventsFirebaseRef = () => {

}