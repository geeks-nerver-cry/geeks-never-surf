import { Reducer } from '../../base/reducer';
import { RECEIVE_EVENT, DETAIL_EVENT, CANCEL_EVENT, SAVED_EVENT, RECEIVE_EVENT_TYPE, RECEIVE_GAME, SavedEvent, SAVED_GAME, SavedGame } from './action-creator';
import { Event, EventDTO, mapToEvent, defaultEvent, GameDTO, Game, mapToGame } from './model';
import { DataSnapshot } from '../../base/firebase';
import customHistory from '../../base/history';


// Event
export type EventState = { [key: string]: Event };

const EventReducer: Reducer = (state: EventState, action) => {
    let newState: EventState;

    switch (action.type) {
        case SAVED_EVENT:
            const savedEvent: SavedEvent = action.data;
            newState = pushInState(state, savedEvent.event, savedEvent.key, true);
            customHistory.push({
                pathname: `${savedEvent.key}`
            });
            break;


        case RECEIVE_EVENT:
            // Fusionner les informations de l'event avec celles déjà présente.
            // Pensez à conserver le isActive
            const receiveEvent: DataSnapshot<EventDTO> = action.data;
            newState = pushInState(state, mapToEvent(receiveEvent.val()), receiveEvent.key);
            break;

        case CANCEL_EVENT:
            newState = Object.assign({}, state);
            break;

        default:
            newState = state || {};
    }

    return newState;
};

const pushInState = (state: EventState, event: Event, key: string, activeEvent?: boolean) => {
    const currentValue = state[key];
    let isActive = activeEvent === true;
    if (!activeEvent && currentValue) {
        isActive = currentValue.isActive;
    }
    return Object.assign({}, state, {
        [key]: Object.assign({}, currentValue, event, { isActive })
    });
};

// Event type
export type EventTypeState = string[];

const EventTypeReducer: Reducer = (state: EventTypeState, action) => {
    let newState: EventTypeState;

    switch (action.type) {
        case RECEIVE_EVENT_TYPE:
            newState = [...state, action.data.val()]
            break;

        default:
            newState = state || [];
    }

    return newState;
};

// Game
export type GameState = Game[];

const GameReducer: Reducer = (state: GameState, action) => {
    let newState: GameState;

    switch (action.type) {
        case RECEIVE_GAME:
            const newGame: DataSnapshot<GameDTO> = action.data;
            newState = [...state.filter(g => g.id !== newGame.key), mapToGame(newGame.val(), newGame.key)]
            break;

        case SAVED_GAME:
            newState = [...state.filter(g => g.id !== newGame.key), mapToGame(newGame.val(), newGame.key)]
            break;

        default:
            newState = state || [];
    }

    return newState;
};


export {
    EventReducer,
    EventTypeReducer,
    GameReducer
}