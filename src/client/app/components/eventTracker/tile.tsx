import * as React from "react";
import './style.less';
import { RouterProps } from "../../base/router";
import { Event } from "./model";
import { Link } from "react-router-dom";

export interface TileProps { 
    event: Event,
    eventId: string,
    clickHandler: () => void
}

export class Tile extends React.Component<TileProps, undefined> {
    render() {
        const event = this.props.event;
        return (
            <div className={'tile ' + (event.isActive ? 'active' : '')} onClick={this.props.clickHandler}>
                <Link to={`/event-tracker/${this.props.eventId}`}>{event.name}</Link>
            </div>
        );
    }
}