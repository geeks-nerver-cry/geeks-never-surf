import * as moment from 'moment';

interface EventBase {
    name?: string;
    games?: string[];
    tag?: string;
}

export interface Event extends EventBase {
    date?: moment.Moment;
    isActive?: boolean;
}

export const defaultEvent: Event = {
    date: moment(),
    games: [],
    isActive: false,
    name: '',
    tag: ''
}

export interface EventDTO extends EventBase {
    date: string;
}


export const mapToEvent = (input: EventDTO, active = false): Event => {
    return Object.assign({}, defaultEvent, input, {
        date: moment(input.date),
        tag: input.tag,
        isActive: active
    });
}

export const mapToEventDTO = (input: Event): EventDTO => {
    return {
        name: input.name,
        date: (input.date || moment()).format(),
        games: input.games,
        tag: input.tag,
    };
}




export interface GameDTO {
    name: string;
}

export const defaultGame: Game = {
    id: '',
    name: ''
}


export interface Game {
    id: string;
    name: string;
}

export const mapToGame = (input: GameDTO, id: string): Game => {
    return {
        id,
        ...input
    };
}

export const mapToGameDTO = (input: Game): GameDTO => {
    return {
        name: input.name
    };
}
