import * as React from "react";
import { Game } from "./model";
import "./game-tiles-style.less";

interface GameChoice extends Game {
    isSelected: boolean;
}


interface GameTilesProps {
    options: Game[];
    selections: string[];
    handleDelete: (gameId: string) => void;
    handleAddition: (gameId: string) => void;
    handleNewGame: (gameName: string) => void
}

interface GameTilesState {
    newGameName: string;
    newGameActive?: boolean;
    unselectedGamesDisplayed: boolean;
}

// 'EventTrackerProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
export class GameTiles extends React.Component<GameTilesProps, GameTilesState> {

    public constructor(props: GameTilesProps) {
        super(props);
        this.state = { newGameName: '', newGameActive: false, unselectedGamesDisplayed: true };
    }

    public handleChange(event: React.FormEvent<HTMLInputElement>) {
        this.setState({ newGameName: event.currentTarget.value });
    }

    public handleAddGame() {
        this.setState({ newGameName: '', newGameActive: false });
        this.props.handleNewGame(this.state.newGameName);
    }

    public componentWillReceiveProps(newProps: GameTilesProps) {
        // this.computeGameState(newProps);
    }

    public render() {
        const { newGameName } = this.state;
        const { handleAddition, handleDelete, options, selections } = this.props;

        const games: GameChoice[] = options.map(g => { return { ...g, isSelected: selections.some(s => s === g.id) } });
        const handleListChange = (game, isSelected) => isSelected ? handleAddition(game.id) : handleDelete(game.id);
        return (
            <div className='game-tiles'>
                <div className="game-list-wrapper">
                    <div className="game-list">
                        {
                            games.map(game => { return <GameTile game={game} handleListChange={(game, isSelected) => handleListChange(game, isSelected)} key={game.id} /> })
                        }
                        <div className={`game-tile new-game ${this.state.newGameActive ? 'active' : ''}`}>
                            <div className="new-game-icon" onClick={() => this.setState({ newGameActive: !this.state.newGameActive })}>
                                <i className="fa fa-plus"></i>
                            </div>
                            <div className="new-game-form">
                                <input type="text" placeholder="Mon super nouveau jeu ..." className='new-game-name' value={newGameName} onChange={(event) => this.handleChange(event)} />
                                <div className="button-wrapper">
                                    <button className="btn-primary" onClick={() => this.handleAddGame()}><i className="fa fa-check"></i></button>
                                    <button className="btn-primary" onClick={() => this.setState({ newGameName: '', newGameActive: false } as GameTilesState)}><i className="fa fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

class GameTile extends React.Component<{ game: GameChoice, handleListChange: (game: Game, isSelected: boolean) => void }, never> {
    public render() {
        const { game, handleListChange } = this.props;
        return (
            <div className={`game-tile ${game.isSelected ? 'active' : ''}`} data-game-id={game.id} onClick={() => handleListChange(game, !game.isSelected)}>
                <header>{game.name}</header>
                <div className="body"></div>
            </div>
        );
    }
}