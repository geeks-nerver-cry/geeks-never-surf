import * as React from "react";
import { EventState } from "./reducer";
import { Tile } from "./tile";
import { Event, Game } from './model';
import { WithContext as ReactTags } from 'react-tag-input';
import './tags.css';
import { Link } from "react-router-dom";
import { GameTiles } from "./games-tiles";
import moment = require("moment");

/**
 * Représente les données envoyées aux composants
 */
export interface EventDetailsProps {
    event: Event;
    eventId: string;
    suggestions: Game[];
    saveHandler: (eventId: string, event: any) => void;
    cancelHandler: () => void;
    handleAddGame: (game: Game) => void;
}

// 'EventTrackerProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
export class Details extends React.Component<EventDetailsProps, Event> {

    public constructor(props: EventDetailsProps) {
        super(props);
    }

    public componentWillMount() {
        this.setState(this.prepareState(this.props.event));
    }

    public componentWillReceiveProps(newProps: EventDetailsProps) {

        if (!this.props.event.tag) {
            this.props.event.tag = this.tagalize(this.props.event.name);
        }

        this.setState(this.prepareState(newProps.event));
    }


    public detailsChange(event: any) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    public render() {
        const event = this.state;
        const { handleAddGame } = this.props;

        return (
            <div className="event-details">
                <div className="details-content">
                    <div className="field-row">
                        <div className="field-block event-name">
                            <label htmlFor="">Nom</label>
                            <input type="text" name='name' value={event.name} onChange={(event) => {
                                this.setState({
                                    name: event.target.value,
                                    tag: this.tagalize(event.target.value),
                                })
                            }} />
                        </div>
                        <div className="field-block">
                            <label htmlFor="">Date</label>
                            <input type="date" name='date' value={event.date.format('YYYY-MM-DD')} onChange={(event) => {
                                this.setState({
                                    date: moment(event.target.value)
                                })
                            }} />
                        </div>
                    </div>
                    <div className="field-row">
                        <div className="field-block event-games">
                            <label htmlFor="">Jeux</label>
                            <GameTiles
                                options={this.props.suggestions}
                                selections={event.games}
                                handleDelete={(gameId: string) => this.handleGameDelete(gameId)}
                                handleAddition={(gameId: string) => this.handleGameAddition(gameId)}
                                handleNewGame={(name: string) => handleAddGame({ id: undefined, name })} />
                        </div>
                    </div>
                </div>
                <footer>
                    <span className="event-tag">Tag : <span className="tag">#{event.tag}</span></span>
                    <div className="btn-wrapper">
                        <Link to='/event-tracker' className="btn" data-action="cancel-form"><i className='fa fa-times'></i>Annuler</Link>
                        <button className="btn btn-primary" data-action="validate-form" onClick={() => this.props.saveHandler(this.props.eventId, this.state)}>
                            <i className='fa fa-check'></i>Valider les changements
                    </button>
                    </div>
                </footer>
            </div>
        );
    }

    public handleGameAddition(gameId: string): void {
        const event = this.state;
        event.games.push(gameId);
        this.setState(event);
    }

    public handleGameDelete(gameId: string): void {
        const event = this.state;
        const gameIndex = event.games.indexOf(gameId);
        event.games.splice(gameIndex, 1);
        this.setState(event);
    }

    private tagalize(name: string): string {
        return name.toLocaleLowerCase().replace(/[^a-zA-Z0-9]/gm, '-');
    }

    private prepareState(event: Event) {
        return {
            ...event,
            tag: event.tag || this.tagalize(event.name)
        };
    }
}