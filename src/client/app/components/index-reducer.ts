import HelloReducer from './home/reducer';
import { StatLoLReducer, LoLChampionsReducer } from './statLoL/reducer';
import AuthReducer from './auth/reducer';
import MenuReducer from './basePage/reducer';
import { EventReducer, EventTypeReducer, GameReducer } from './eventTracker/reducer';
import LogReducer from './logger/reducer';

export default [
    HelloReducer,
    StatLoLReducer,
    LoLChampionsReducer,
    AuthReducer,
    MenuReducer,
    EventReducer,
    EventTypeReducer,
    LogReducer,
    GameReducer,
];