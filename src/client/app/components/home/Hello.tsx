import * as React from "react";
import './style.less';
import { RouterProps } from "../../base/router";

export interface HelloProps { compiler: string; framework: string; }

// 'HelloProps' describes the shape of props.
// State is never set so we use the 'undefined' type.
export class Hello extends React.Component<RouterProps, undefined> {
    render() {
        const state = this.props.state.Hello;
        const actions = this.props.actions;

        return (
            <div className='component-hello'>
                {/* <h1 className='component-hello'>Hello from {state && state.compiler} and {state && state.framework}!</h1>
                <button onClick={() => {
                    actions.sayHello();
                    actions.logDev('pouet');
                }}>Dis bonjour !</button> */}

                <p className="title">Bonjour à tous !</p>
                <p>Vous trouverez ici tout ce que j'ai eut le temps de développez pour notre petite commuanuté (déjà plus grande que la commuanuté de l'anneau :p).</p>
                <p>Si vous voyez des amélioration n'hésité pas à m'en faire part.</p>
                <p>Si vous voyez un bug ... corrigez le !</p>
                <p>Pour finir un remerciement à tous ceux qui m'ont aidé à mettre ce site en place après une très longue gestation.</p>
                <p>Enjoy ;)</p>
            </div>
        );
    }
}