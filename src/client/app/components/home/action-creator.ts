import { Action } from "../../base/reducer";
import { HelloProps } from "./Hello";

export const sayHello = function() {
    
    let action: Action<HelloProps> = {
        type: 'SAY_HELLO',
        data: {
            compiler: "TypeScript",
            framework: "React"        
        }
    };

    return action;
};