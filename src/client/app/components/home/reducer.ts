import { Reducer, Action } from "../../base/reducer";
import { HelloProps } from "./Hello";

const HelloReducer: Reducer = (state, action: Action<HelloProps>) => {

    switch (action.type) {
        case 'SAY_HELLO':
            return {
                ...state,
                compiler: action.data.compiler,
                framework: action.data.framework,
            };
    
        default:
            return state;
    }
};

export default HelloReducer;