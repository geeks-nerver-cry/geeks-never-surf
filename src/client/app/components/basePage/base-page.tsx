import * as React from 'react';
import '../../base/reset.less';
import './style.less';
import { Link } from 'react-router-dom';
import { RouterProps } from '../../base/router';
import { Header } from './header';
import { Menu } from './menu';
import { Auth } from '../auth/auth';
import { MenuState } from './reducer';

export interface BaseProps {
    actions: any,
    state: any,
};

export class BasePage extends React.Component<BaseProps, undefined> {
    render() {
        const children:any = this.props.children;
        const toggleMenu = () => { this.props.actions.menuPositionChanged(this.props.state.Menu.isCollapsed); }
        const user = this.props.state.Auth && this.props.state.Auth.user || undefined;
        const selectedUrl = document.location.pathname;
        const menuState: MenuState = this.props.state.Menu;
        return (
            <div className="site-wrapper">
                {this.props.state.Auth && this.props.state.Auth.user &&
                    <Header toggleMenu={toggleMenu} signOut={this.props.actions.signOut} />
                }
                {user &&
                <main>
                    <aside className={(menuState.isCollapsed ? "reduce" : "" )}>
                        <Menu selectedUrl={selectedUrl} />
                    </aside>
                    {user && children}
                </main>
                }
                {user === undefined &&
                    <Auth actions={this.props.actions} state={this.props.state}></Auth>
                }
                <footer></footer>
            </div>
        );
    }
}