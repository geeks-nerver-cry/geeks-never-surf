import { Reducer } from '../../base/reducer';
import {MENU_POSITION_CHANGED} from './action-creator';

export interface MenuState {
    isCollapsed: boolean
}

const MenuReducer: Reducer = (state: MenuState, action) => {
    let newState: MenuState;

    switch (action.type) {
        case MENU_POSITION_CHANGED:
            newState = {
                isCollapsed: !state.isCollapsed
            }
            break;
        default:
            newState = state || {
                isCollapsed: true
            };
    }

    return newState;
};

export default MenuReducer;