import * as React from 'react';
import './style.less';
import { UserInfoLight } from '../userInfo/user-info-light';

export interface HeaderProps {
    toggleMenu: () => void,
    signOut: () => void,
}

export class Header extends React.Component<HeaderProps, undefined> {
    render() {
        return (
            <header>
                <span className="btn-menu" data-action="open-menu" onClick={this.props.toggleMenu}>
                    <i className="fa fa-bars"></i>
                </span>
                <i className="brand">Geeks Never Cry</i>
                <div className="header-filler"></div>
                <UserInfoLight signOut={this.props.signOut} />
            </header>
        );
    }
}