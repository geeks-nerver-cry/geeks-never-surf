import * as React from 'react';
import './style.less';
import { Link } from 'react-router-dom';

export interface MenuItemProps { 
    isSelected: boolean,
    iconClass: string,
    label: string,
    url: string
 }

export class MenuItem extends React.Component<MenuItemProps, undefined> {
    render() {
        return (
            <Link to={this.props.url} className={this.props.isSelected ? 'selected': ''}>
                <i className={"fa " + this.props.iconClass}></i>
                {this.props.label}
            </Link>
        );
    }
}