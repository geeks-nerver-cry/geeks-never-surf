import { Action } from "../../base/reducer";

export const MENU_POSITION_CHANGED = 'MENU_POSITION_CHANGED';
export const menuPositionChanged = (): Action<any> => {
    return {
        type: MENU_POSITION_CHANGED
    };
};