import * as React from 'react';
import './style.less';
import { Link } from 'react-router-dom';
import { MenuItem } from './menu-item';

interface MenuItemsInfo {
    iconClass: string,
    url: string,
    label: string,
    order: number,
    isActive: boolean,
    pattern: RegExp,
}

const menuItemInfos: MenuItemsInfo[] = [
    {
        iconClass: 'fa-home',
        label: 'Home',
        url: '/',
        pattern: /^\/$/,
        order: 0,
        isActive: true
    },{
        iconClass: 'fa-bookmark',
        label: 'LoL',
        url: '/stat-lol',
        pattern: /^\/stat-lol$/,
        order: 2,
        isActive: false
    },{
        iconClass: 'fa-calendar',
        label: 'Evénement',
        url: '/event-tracker',
        pattern: /^\/event-tracker\/?([a-zA-Z0-9-_])*$/,
        order: 1,
        isActive: true
    }
];

export interface MenuProps {
    selectedUrl: string
}

export class Menu extends React.Component<MenuProps, undefined> {
    getItemsToRender () {
        return menuItemInfos.filter(item => item.isActive).sort((a,b) => a.order - b.order);
    }

    render() {
        const url = this.props.selectedUrl;
        return (
            <nav id="#menu">
                <ul>
                    {
                        this.getItemsToRender().map((item, i) => {
                            return (
                                <li key={i}>
                                    <MenuItem isSelected={item.pattern.test(url)} iconClass={item.iconClass} label={item.label} url={item.url} />
                                </li>
                            )
                        })
                    }
                </ul>
            </nav>
        );
    }
}