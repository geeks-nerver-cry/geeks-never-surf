import { Action } from "../../base/reducer";
import { auth, database, FirebaseEvent, ReferencePathType } from '../../base/firebase';
import { logError } from "../logger/action-creator";

export enum AuthActionType {
    RequestingCredential = 'REQUESTING_CREDENTIAL',
    ReceiveCredential = 'RECEIVE_CREDENTIAL',
    SignOut = 'SIGN_OUT',
}


export const requestingCredential = function(userName: string) {

    let action: Action<{userName: string}> = {
        type: AuthActionType.RequestingCredential,
        data : {
            userName
        }
    };

    return action;
};


export const receiveCredential = function(user: any) {

    let action: Action<any> = {
        type: AuthActionType.ReceiveCredential,
        data : {
            user
        }
    };

    return action;
};


export const requestCredential = function(userName: string, userPassword: string) {

    return (dispatch: any) => {

        dispatch(requestingCredential(userName));

        auth.signInWithEmailAndPassword(userName, userPassword)
            .then((result) => {
                console.log(result);
                dispatch(receiveCredential(result.u));
            })
            .catch((reason) => {
                dispatch(logError(reason.message));
            });
    };
};

export const signOut = function(userName: string) {

    auth.signOut();

    let action: Action<{userName: string}> = {
        type: AuthActionType.SignOut,
    };

    return action;
};

export function subcribeEvent() {

    return (dispatch: (action: Action<any>) => void) => {

        //#region Event
        // Reference to the /users/ database path.
        const usersRef = database.ref(ReferencePathType.Users);

        // Make sure we remove all previous listeners.
        usersRef.off();

        // Loads events and listen for new ones.
        // usersRef.on(FirebaseEvent.ChildAdded, data => dispatch(receiveEvent(data)));
        // usersRef.on(FirebaseEvent.ChildChanged, data => dispatch(receiveEvent(data)));
        //#endregion

    };
};
