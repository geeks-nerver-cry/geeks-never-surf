import * as React from 'react';
import './style.less';
import { Toastr } from '../logger/toastr';
import { RouterProps } from '../../base/router';

const cursedWords = [
    'Faut pas s´y fier, car personne n´en est ressorti ! ... Faut dire aussi que personne y est entré.',
    'Je savais bien que tu étais une salope !',
    'J´irai bien à l´église pour jouer du trombone',
    'Mais qui est ce petit personnage ?',
];

export interface AuthProps {
    actions: any,
    state: any
};

/**
 * Composant de connexion
 */
export class Auth extends React.Component<AuthProps, undefined> {
    private authUserNameId = 'auth-username';
    private authUserNamePassword = 'auth-password';

    private goAuth() {
        const inputUserName: any = document.getElementById(this.authUserNameId);
        const inputUserPassword: any = document.getElementById(this.authUserNamePassword);

        this.props.actions.requestCredential(inputUserName.value, inputUserPassword.value);
    }

    public render() {
        const authState = this.props.state.Auth;

        const reason = cursedWords[Math.floor(Math.random() * Math.floor(cursedWords.length))];

        return (
            <div className="auth">
                <div className="container-auth-form">
                    <h1>Geeks Never Cry</h1>
                    <form className="auth-form">
                        <div className="field-part">
                            <div className="input-wrapper">
                                <input type="text" id={this.authUserNameId} placeholder="Email" onKeyPress={(e) => e.key === 'Enter' && this.goAuth()} />
                                <i className="fa fa-user-o"></i>
                            </div>
                            <div className="input-wrapper">
                                <input type="password" id={this.authUserNamePassword} placeholder="Mot de passe" onKeyPress={(e) => e.key === 'Enter' && this.goAuth()} />
                                <i className="fa fa-lock"></i>
                            </div>
                        </div>
                        <footer className="">
                            <button className="btn-cancel" type="reset" onClick={() => this.props.actions.logDev(reason)}>Retourner à ma vie</button>
                            <button className="btn-success" type="button" onClick={() => this.goAuth()} data-action="validate">Entrer</button>
                        </footer>
                    </form>
                </div>
                <Toastr state={this.props.state} actions={this.props.actions} />
            </div>
        );
    }
}