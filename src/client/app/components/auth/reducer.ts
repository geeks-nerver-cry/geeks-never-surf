import { Reducer } from "../../base/reducer";
import { AuthActionType } from './action-creator';
import * as Cookies from 'js-cookie';

const AuthReducer: Reducer = (state, action) => {

    let newState: any;
    switch (action.type) {
        case AuthActionType.RequestingCredential:
            newState = Object.assign({}, state, {
                isLoading: true
            });
            break;

        case AuthActionType.ReceiveCredential:
            newState = Object.assign({}, {
                user: {
                    uid: action.data.uid
                }
            });
            Cookies.set('firebase-user', JSON.stringify(newState));
            break;

        case AuthActionType.SignOut:
            Cookies.remove('firebase-user');
            newState = {};
            break;

        default:
            newState = state || {};
    }

    return newState;
};

export default AuthReducer;