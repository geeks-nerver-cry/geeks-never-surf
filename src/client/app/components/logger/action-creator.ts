import { Action } from "../../base/reducer";
import * as moment from 'moment';

export interface LogData {
    time: string,
    level: LogLevel,
    message: string
}

export type LogAction = Action<LogData>;

export enum LogLevel {
    Error,
    Warn,
    Info,
    Dev,
    Success,
}

export const LOG = 'LOG';

const logFactory = (actionType: string, level: LogLevel, message: string) => {

    // Manque le dispatcher
    const time = moment().format('x');

    return (dispatch: (action: Action<any>) => LogAction) => {
        setTimeout(() => dispatch(removeLog(time)), 2000);

        dispatch({
            type: actionType,
            data: {
                time,
                level,
                message
            }
        });
    };
};

export const REMOVE_LOG = 'REMOVE_LOG';
export const removeLog = (logKey: string): Action<string> => {
    return {
        type: REMOVE_LOG,
        data: logKey
    }
}

export const logError = (message: string) => {
    return logFactory(LOG, LogLevel.Error, message);
}

export const logWarn = (message: string) => {
    return logFactory(LOG, LogLevel.Warn, message);
}

export const logInfo = (message: string) => {
    return logFactory(LOG, LogLevel.Info, message);
}

export const logDev = (message: string) => {
    return logFactory(LOG, LogLevel.Dev, message);
}

export const logSuccess = (message: string) => {
    return logFactory(LOG, LogLevel.Success, message);
}