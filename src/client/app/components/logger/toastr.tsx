import * as React from "react";
import './style.less';
import { LogState } from "./reducer";
import { LogLevel } from "./action-creator";
import { Transition, CSSTransition, TransitionGroup } from 'react-transition-group'

interface ToastProp {
    message: string;
    level: LogLevel;
    key: string;
}

const Fade = (childrenProps: any) => {
    const { children, ...props } = childrenProps;
    return (
        <CSSTransition
            {...props}
            timeout={200}
            classNames="fade"
            transitionAppear={true}
            transitionLeave={true}
            transitionEnterTimeout={800}
            transitionAppearTimeout={800}
            transitionLeaveTimeout={200}
        >
            {children}
        </CSSTransition>
    );
};

const Toast = (props: ToastProp) => {
    const { message, level } = props;
    let title;
    let icon;
    let toastClass;
    switch (level) {
        case LogLevel.Dev:
            title = "Just for you";
            icon = "fa-bug";
            toastClass = 'toast-dev';
            break;

        case LogLevel.Info:
            title = "Ca peut aider ...";
            icon = "fa-comment";
            toastClass = 'toast-info';
            break;

        case LogLevel.Warn:
            title = "C'est chaud !";
            icon = "fa-exclamation-triangle";
            toastClass = 'toast-warn';
            break;

        case LogLevel.Error:
            title = "Oups :/";
            icon = "fa-meh-o";
            toastClass = 'toast-error';
            break;

        case LogLevel.Success:
            title = "GG ;)";
            icon = "fa-thumbs-o-up";
            toastClass = 'toast-success';
            break;
    }

    return (
        <div className={`toast ${toastClass}`}>
            <div className="icon-wrapper">
                <i className={`icon fa ${icon}`}></i>
            </div>
            <div className="text">
                <span className="title">{title}</span>
                <span className="message">{message}</span>
            </div>
        </div>
    );
};

interface ToastrProps {
    state: any;
    actions: any;
}

export class Toastr extends React.Component<ToastrProps, any> {
    render() {
        const state: LogState = this.props.state.Log;
        return (
            <TransitionGroup className="toastr">
                {state &&
                    Object.keys(state).map(key => {
                        const { message, level } = state[key];
                        return (
                            <Fade key={key}>
                                <Toast key={key} message={message} level={level} />
                            </Fade>
                        );
                    })
                }
            </TransitionGroup>
        );
    }
} 1