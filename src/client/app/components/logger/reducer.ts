import { Reducer } from '../../base/reducer';
import { LOG, LogAction, LogLevel, LogData, REMOVE_LOG } from './action-creator';

export interface LogState {
    [key: string]: LogData
}

const LogReducer: Reducer = (state = {}, action) => {

    let newState: LogState;

    switch (action.type) {

        case LOG:
            const { time, message, level } = action.data;
            newState = Object.assign({}, state, {
                [time]: action.data
            });

            break;
        
        case REMOVE_LOG:
            newState = {};
            Object.keys(state).forEach(key => {
                if (key !== action.data) {
                    newState[key] = state[key];
                }
            })
            break;

        default:
            newState = state || {};
    }

    return newState;
}

export default LogReducer;