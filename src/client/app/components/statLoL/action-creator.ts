import { Action } from "../../base/reducer";

const urlRoot = 'http://localhost:3000/api';

export const REQUESTING_MATCHES = 'REQUESTING_MATCHES';
export const requestingMatches = function(summonerName: String) {
    
    let action: Action<any> = {
        type: REQUESTING_MATCHES
    };

    return action;
};

export const RECEIVE_MATCHES = 'RECEIVE_MATCHES';
export const receiveMatches = function(summonerName: String, matches: any) {
    
    let action: Action<any> = {
        type: RECEIVE_MATCHES,
        data : {
            summonerName,
            matches
        }
    };

    return action;
};

export const requestMatches = function(summonerName: String) {
    
    return (dispatch: any) => {

        dispatch(requestingMatches(summonerName));

        fetch(urlRoot + '/riot/info/' + summonerName)
            .then(response => response.json())
            .then(json => dispatch(receiveMatches(summonerName, json)));
    };
};

export const requestChampions = function() {
    
    return (dispatch: any) => {
        console.log('pouet');
        dispatch(requestingChampions());

        fetch(urlRoot + '/riot/champion')
            .then(response => response.json())
            .then(json => dispatch(receiveChampions(json)))
            .catch(error => dispatch(receiveChampions(error)));
    };
};

export const REQUESTING_CHAMPIONS = 'REQUESTING_CHAMPIONS';
export const requestingChampions = function() {
    
    let action: Action<any> = {
        type: REQUESTING_CHAMPIONS
    };

    return action;
};

export const RECEIVE_CHAMPIONS = 'RECEIVE_CHAMPIONS';
export const receiveChampions = function(data: any) {
    
    let action: Action<any> = {
        type: RECEIVE_CHAMPIONS,
        data
    };

    return action;
};