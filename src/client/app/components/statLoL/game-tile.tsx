import * as React from 'react';
import './style.less';

import * as moment from 'moment';

export interface GameTileProps { 
    matchId: number,
    timestamp: number,
    isLoading: Boolean,
    winOrLoseAgain: Boolean,
    key: number,
    imageUrl: string
}

export class GameTile extends React.Component<GameTileProps, undefined> {
    render() {
        const dateString = moment(this.props.timestamp).format('DD/MM/YYYY HH:mm');
        const url = this.props.imageUrl;
        return (
            <div className="game-tile" data-match-id={this.props.matchId}>
                <i className="game-date">{dateString}</i>
                <img src={url} alt=""/>
            </div>
        );
    }
}