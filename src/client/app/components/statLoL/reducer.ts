import { Reducer } from "../../base/reducer";
import { RECEIVE_MATCHES, REQUESTING_MATCHES, RECEIVE_CHAMPIONS, REQUESTING_CHAMPIONS } from './action-creator';

const StatLoLReducer: Reducer = (state, action) => {

    let newState: any;
    switch (action.type) {
        case REQUESTING_MATCHES: 
            newState = Object.assign({}, state, {
                isLoading: true
            });
            break;

        case RECEIVE_MATCHES:
            newState = Object.assign({}, state, {isLoading: false}, action.data);
            break;
            
        default:
            newState = state;
    }

    return newState;
};
export {StatLoLReducer};


const LoLChampionsReducer: Reducer = (state, action) => {
    
    let newState: any;
    switch (action.type) {

        case REQUESTING_CHAMPIONS: 
            newState = {
                isLoading: true
            };
            break;
            

        case RECEIVE_CHAMPIONS:
            newState = {data: action.data};
            break;
            
        default:
            newState = state;
    }

    return newState;
};
export {LoLChampionsReducer};
