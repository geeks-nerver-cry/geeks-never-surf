import * as React from "react";
import './style.less';
import { RouterProps } from "../../base/router";
import { GameTile } from "./game-tile";

export interface StatLoLProps { 
    
}

export class StatLoL extends React.Component<RouterProps, undefined> {
    computeChampionUrl(versionApi: String, championImageName: String) {
        return 'https://ddragon.leagueoflegends.com/cdn/' + versionApi + '/img/champion/' + championImageName;
    }

    componentDidUpdate(prevProps: RouterProps) {
        const champions = prevProps.state.LoLChampions;
        const actions = prevProps.actions;

        if(!champions || (champions && !champions.data && !champions.isLoading)) {
            actions.requestChampions();
        }
    }

    render() {
        const state = this.props.state.StatLoL;
        const champions = this.props.state.LoLChampions;
        const actions = this.props.actions;

        const clickHandler = () => {
            const input:any = (document.getElementById('summoner-name'));
            actions.requestMatches(input.value)
        };

        return (
            <div id="stat-lol">
                <div className="search">
                    <input type="text" name="summonerName" id="summoner-name" autoFocus/>
                    <button onClick={clickHandler}><i className="fa fa-search"></i></button>
                </div>
                <div className="matches">
                    {state && state.matches &&
                        state.matches.matches.map((match: any, i: number) => {
                            return <GameTile isLoading={false} matchId={match.gameId} timestamp={match.timestamp} winOrLoseAgain={false} imageUrl={this.computeChampionUrl(champions.data.version, champions.data.data[match.champion].image.full)} key={i}/>
                        })
                    }
                </div>
            </div>
        );
    }
}