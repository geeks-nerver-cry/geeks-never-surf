import * as React from "react";
import * as ReactDOM from "react-dom";

import './style/main.less';

import { Router, Route } from 'react-router-dom';
import history from './base/history';

import { subscribe, actionDispatcher } from './store';

// Components
import { Hello, HelloProps } from "./components/home/Hello";
import { BasePage } from './components/basePage/base-page';

import { Action } from "./base/reducer";
import { PropsRoute } from "./base/props-route";
import { actions } from "./root/main-action-creator";
import { StatLoL } from "./components/statLoL/stat-lol";
import { EventTracker } from './components/eventTracker/event-tracker';
import { Toastr } from "./components/logger/toastr";



subscribe((state: any) => {
    const router = (
        <Router history={history} >
            <BasePage actions={actions} state={state} >
                <div id="content">
                    <PropsRoute exact path="/" component={Hello} state={state} actions={actions} ></PropsRoute>
                    <PropsRoute exact path="/stat-lol" component={StatLoL} state={state} actions={actions} ></PropsRoute>
                    <PropsRoute exact path="/event-tracker" component={EventTracker} state={state} actions={actions} ></PropsRoute>
                    <PropsRoute exact path="/event-tracker/:id" component={EventTracker} state={state} actions={actions} ></PropsRoute>
                </div>
                <Toastr state={state} actions={actions} />
            </BasePage>
        </Router>
    );

    ReactDOM.render(
        router,
        document.getElementById("main")
    );
});