import { createBrowserHistory, History } from 'history';
import {subscribe} from '../store';

export interface RxHistory extends History {
    state?: any,
}

const customHistory: RxHistory = createBrowserHistory();

subscribe((state) => {
    customHistory.state = state;
});

export default customHistory;