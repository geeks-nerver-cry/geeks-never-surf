export interface Action<TData> {
    type: string;
    data?: TData;
};

export type Reducer = (state:any, action:Action<any>) => any;