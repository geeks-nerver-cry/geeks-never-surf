import * as React from "react";
import * as ReactDOM from "react-dom";
import { Route } from 'react-router-dom';

const renderMergedProps = (component: any, ...rest:any[]) => {
    const finalProps = Object.assign({}, ...rest);
    return (
      React.createElement(component, finalProps)
    );
  }

export class PropsRoute extends React.Component<any, undefined> {
    render() {
        const {component, ...rest} = this.props;
        return <Route {...rest} render={routeProps => renderMergedProps(component, routeProps, rest) }/>;
    }
}