import * as firebase from 'firebase'
import { Action } from './reducer';
const config = {
    apiKey: "AIzaSyBdX1YVFp0EAqQLJYPtI0mUCWbNRT5Ec2I",
    authDomain: "geeks-never-cry.firebaseapp.com",
    databaseURL: "https://geeks-never-cry.firebaseio.com",
    projectId: "geeks-never-cry",
    storageBucket: "geeks-never-cry.appspot.com",
    messagingSenderId: "291071877015"
};
const app = firebase.initializeApp(config);
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export const database = firebase.database(app);
export default firebase;

/**
 * A snapshot of a database element
 */
export interface DataSnapshot<TValue> extends firebase.database.DataSnapshot {
    val: () => TValue;
}

export enum FirebaseEvent {
    ChildAdded = 'child_added',
    ChildRemoved = 'child_removed',
    ChildChanged = 'child_changed',
    ChildMoved = 'child_moved'
}

export enum ReferencePathType {
    Events = 'events',
    EventTypes = 'event-types',
    Games = 'games',
    Users = 'users',
}

const refs: { [key: string]: firebase.database.Reference } = {};

export function getReference(referencePathType: ReferencePathType): firebase.database.Reference {
    if (!refs[referencePathType]) {
        refs[referencePathType] = database.ref(referencePathType);
    }

    return refs[referencePathType];
}


export function receiveData<TDto, TData extends DataSnapshot<TDto>> (type: string, data: TData): Action<TData> {
    return {
        type,
        data,
    };
};