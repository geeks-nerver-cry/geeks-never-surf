import { RxHistory } from "./history";
import { Props } from "react";

export interface RouterProps extends Props<any>{
    history: RxHistory,
    state: any;
    actions: any;
    match: {
        params: any
    }
}