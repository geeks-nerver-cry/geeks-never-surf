import { Subject } from '@reactivex/rxjs';
import { mainReducer } from './root/main-reducer';
import { Action } from './base/reducer';
import * as Cookies from 'js-cookie';
import { subscribeOn } from '@reactivex/rxjs/dist/cjs/operator/subscribeOn';

let lastState: any;
const subscribers: ((state: any) => void)[] = [];

const action$ = new Subject();

const authState = Cookies.get('firebase-user');
// Initial State
const initState:any = {
    Auth: authState && JSON.parse(authState)
};
const initialAction: Action<void> = {
    type: 'INITIALIZE'
};

// Reduxification
const store$ = action$  
                .startWith(mainReducer(initState, initialAction))
                .scan(mainReducer);

store$.subscribe(execSubscribers);

function execSubscribers(state: any) {
    lastState = state;
    subscribers.forEach(s => s(state));
}

export function subscribe(action: (state: any) => void) {
    subscribers.push(action);
    action(lastState);
}


// Higher order function to send actions to the stream
export const actionDispatcher = function (func: any) {
    return (...args: any[]) => dispatch(func(...args));
} 

export const dispatch = function (action: any) {

    if( typeof action === 'function') {
        action(dispatch);
    } else {
        action$.next(action);
    }
};