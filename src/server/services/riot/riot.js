const env = require('./env.json');
const https = require('https');


const getSummonerInfo = function (summonerName) {
    const summonerRequest = new Promise((resolve, reject) =>

    https.get(env.urlRoot + env.urlSummonerByName + summonerName + '?api_key=' + env.apiKey, (resp) => {

        let data = '';

        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
            data += chunk;
        });

        // The whole response has been received. Print out the result.
        resp.on('end', () => {
            resolve(JSON.parse(data));
        });
    }).on("error", (err) => {
        reject(err);
    }));

    const matchRequest = new Promise((resolve, reject) => {
        summonerRequest.then((data) => {
            const url = env.urlRoot + '/lol/match/v3/matchlists/by-account/' + parseInt(data.accountId) + '/recent?api_key=' + env.apiKey;
            https.get(url, (resp) => {
                let data = '';

                // A chunk of data has been recieved.
                resp.on('data', (chunk) => {
                    data += chunk;
                });

                // The whole response has been received. Print out the result.
                resp.on('end', () => {
                    resolve(JSON.parse(data));
                });
            }).on("error", (err) => {
                reject(err);
            });
        });
    });

    return matchRequest;
}

const getMatchInfo = function (matchId) {
    const matchPromise = new Promise((resolve, reject) => {
        https.get(env.urlRoot + '/lol/match/v3/matches/' + matchId + '?api_key=' + env.apiKey, (resp) => {

            let data = '';

            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });

            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                resolve(JSON.parse(data));
            });
        }).on("error", (err) => {
            reject(err);
        });
    });

    return matchPromise;
};

let championData;
const getChampionInfo = function () {

    const championPromise = new Promise((resolve, reject) => {
        if(!championData) {
            https.get(env.urlRoot + '/lol/static-data/v3/champions?locale=fr_FR&tags=image&dataById=true&api_key=' + env.apiKey, (resp) => {

                let data = '';

                // A chunk of data has been recieved.
                resp.on('data', (chunk) => {
                    data += chunk;
                });

                // The whole response has been received. Print out the result.
                resp.on('end', () => {
                    championData = JSON.parse(data);
                    resolve(championData);
                });
            }).on("error", (err) => {
                reject(err);
            });
        } else {
            resolve(championData);
        }


    });

    return championPromise;
}

module.exports = {
    url: env.urlRoot,
    getSummonerInfo,
    getMatchInfo,
    getChampionInfo
}