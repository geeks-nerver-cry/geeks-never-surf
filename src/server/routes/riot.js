var express = require('express');
var router = express.Router();
var bo = require('../services/riot/riot');

/* GET users listing. */
router.get('/info/:summonerName', function(req, res, next) {
  bo.getSummonerInfo(req.params.summonerName).then((data) => {
      res.json(data);
  }).catch((err) => {
    console.log(err);
  });
});

router.get('/match/:matchId', function(req, res, next) {
  bo.getMatchInfo(req.params.matchId).then((data) => {
    res.json(data);
  }).catch((err) => {
    console.log(err);
  });
});

router.get('/champion', function(req, res, next) {
  bo.getChampionInfo().then((data) => {
    res.json(data);
  }).catch((err) => {
    console.log(err);
  });
});

module.exports = router;
