var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cons = require('consolidate');

var riot = require('./routes/riot');

var app = express();

// view engine setup
// app.engine('html', cons.swig)
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/public')));

app.use('/api/riot', riot);
app.use((req, res) => res.sendFile(`${__dirname}/public/index.html`));

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   var err = new Error('Not Found');
//   err.status = 404;
//   next(err);
// });

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err);
});


const accountName = 'geeks.never.cry.service@gmail.com';
const accountPassword = 'Cr0m rit de nous';

console.log('Config sender');
//geeks.never.cry.service@gmail.com
//Cr0m rit de nous
var send = require('gmail-send')({
  //var send = require('../index.js')({
  user: accountName,
  // user: credentials.user,                  // Your GMail account used to send emails
  pass: accountPassword,
  // pass: credentials.pass,                  // Application-specific password
});

const notifyNewEvent = (event) => {
  var htmlContent = 'Oyez brave geek, Un nouvel évènement est prévu !<br /><br />Les jeux propposé sont :<br />';

  event.games.forEach((game) => {
    htmlContent += '- ' + game.text + '<br />';
  });

  htmlContent += '<br />Pensez à en mettre au moins un à jour ;)';
  htmlContent += '<br /><br /><i>Ceci est un message autogénéré. Toute(s) insulte(s) est(sont) l\'oeuvre d\'un ou plusieurs développeurs déséquilibré. Veuillez ne pas l\'en excuser.</i>';

  send({
    // to: 'geeksnevercry@googlegroups.com',
    to: 'mxm.barreau@gmail.com',
    subject: event.name,
    html: htmlContent
  });
}


console.log('Firebase');
var firebase = require('firebase');
var config = {
  apiKey: "AIzaSyBdX1YVFp0EAqQLJYPtI0mUCWbNRT5Ec2I",
  authDomain: "geeks-never-cry.firebaseapp.com",
  databaseURL: "https://geeks-never-cry.firebaseio.com",
  projectId: "geeks-never-cry",
  storageBucket: "geeks-never-cry.appspot.com",
  messagingSenderId: "291071877015"
};
var firebaseApp = firebase.initializeApp(config);
var first = true;

// firebase.auth(firebaseApp).signInWithEmailAndPassword(accountName, accountPassword).then(() => {
//   console.log('firebase signed in');

//   firebase.database(firebaseApp).ref('events').endAt().limitToLast(1).on('child_added', (snapshot) => {
//     if (first) {
//       first = false;
//       console.log('Already exist');
//       return;
//     }
//     console.log('New event: ' + snapshot.val().name);

//     var event = snapshot.val();
//     notifyNewEvent(event);

//   }, () => {
//     console.log('Cancellation');
//   });

// });

module.exports = app;